import Vue from 'vue'
import Entry from '@/components/Entry'

describe('Entry.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Entry)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.insurance-name').textContent)
      .to.equal('国寿财综合意外险')
  })
})

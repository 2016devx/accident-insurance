/**
 * 应用程序常量
 * @date 2017-11-21
 */
export var SERVER_ERROR = '程序员小哥哥开小差了，请稍后重试'
export var QUERY_RISK_PREPEARE_INFO_ERROR = '获取投保预备信息出错'
export var QUERY_JOB_CLASS_ERROR = '获取职业等级出错'

export var APPLICANT_NAME_EMPTY_ERROR = '投保人姓名不能为空'
export var APPLICANT_IDENTITY_EMPTY_ERROR = '投保人身份证号码不能为空'
export var APPLICANT_EMAIL_EMPTY_ERROR = '投保人邮箱不能为空'
export var APPLICANT_PHONE_EMPTY_ERROR = '投保人手机号码不能为空'

// 只要两者关系非本人的时候才用到
export var INSURED_NAME_EMPTY_ERROR = '被保人姓名不能为空'
export var INSURED_IDENTITY_EMPTY_ERROR = '被保人身份证号码不能为空'

export var APPLICANT_NAME_ERROR = '投保人姓名只能为中文'
export var APPLICANT_IDENTITY_ERROR = '投保人身份证号码格式不正确'
export var APPLICANT_EMAIL_ERROR = '投保人邮箱格式不正确'
export var APPLICANT_PHONE_ERROR = '投保人手机号码格式不正确'

// 只要两者关系非本人的时候才用到
export var INSURED_NAME_ERROR = '被保人姓名只能为中文'
export var INSURED_IDENTITY_ERROR = '被保人身份证号码格式不正确'

export var NAME_LENGTH_LIMIT_ERROR = '姓名不能少于2个字'

export var JOB_MISSING_ERROR = '请为被保人选择相应的职业'

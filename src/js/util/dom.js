/**
 * 获取DOM元素的小写形式
 */
export var getNodeName = (el) => el.nodeName.toLowerCase()

/* eslint-disable */
/**
 * @note
 * 以下实现深浅复制的代码是从jQuery中分离出来
 */
var class2type = {}
var toString = class2type.toString
var hasOwn = class2type.hasOwnProperty
var support = {}
var jQuery = {}

jQuery.extend = function () {
  var options, name, src, copy, copyIsArray, clone,
    target = arguments[0] || {},
    i = 1,
    length = arguments.length,
    deep = false;

  // Handle a deep copy situation
  if (typeof target === "boolean") {
    deep = target;

    // Skip the boolean and the target
    target = arguments[i] || {};
    i++;
  }

  // Handle case when target is a string or something (possible in deep copy)
  if (typeof target !== "object" && !jQuery.isFunction(target)) {
    target = {};
  }

  // Extend jQuery itself if only one argument is passed
  if (i === length) {
    target = this;
    i--;
  }

  for (; i < length; i++) {

    // Only deal with non-null/undefined values
    if ((options = arguments[i]) != null) {

      // Extend the base object
      for (name in options) {
        src = target[name];
        copy = options[name];

        // Prevent never-ending loop
        if (target === copy) {
          continue;
        }

        // Recurse if we're merging plain objects or arrays
        if (deep && copy && (jQuery.isPlainObject(copy) ||
          (copyIsArray = jQuery.isArray(copy)))) {

          if (copyIsArray) {
            copyIsArray = false;
            clone = src && jQuery.isArray(src) ? src : [];

          } else {
            clone = src && jQuery.isPlainObject(src) ? src : {};
          }

          // Never move original objects, clone them
          target[name] = jQuery.extend(deep, clone, copy);

          // Don't bring in undefined values
        } else if (copy !== undefined) {
          target[name] = copy;
        }
      }
    }
  }

  // Return the modified object
  return target;
}

jQuery.extend({
  isFunction: function (obj) {
    return jQuery.type(obj) === "function";
  },

  isArray: Array.isArray,

  isWindow: function (obj) {
    return obj != null && obj === obj.window;
  },

  isPlainObject: function (obj) {
    var key;

    // Not plain objects:
    // - Any object or value whose internal [[Class]] property is not "[object Object]"
    // - DOM nodes
    // - window
    if (jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow(obj)) {
      return false;
    }

    // Not own constructor property must be Object
    if (obj.constructor &&
      !hasOwn.call(obj, "constructor") &&
      !hasOwn.call(obj.constructor.prototype || {}, "isPrototypeOf")) {
      return false;
    }

    // Own properties are enumerated firstly, so to speed up,
    // if last one is own, then all properties are own
    for (key in obj) { }

    return key === undefined || hasOwn.call(obj, key);
  },

  type: function (obj) {
    if (obj == null) {
      return obj + "";
    }

    // Support: Android<4.0, iOS<6 (functionish RegExp)
    return typeof obj === "object" || typeof obj === "function" ?
      class2type[toString.call(obj)] || "object" :
      typeof obj;
  }
})

// Populate the class2type map
"Function Array Object".split(" ").forEach(function (name, i) {
  class2type["[object " + name + "]"] = name.toLowerCase();
})

// For CommonJS
export {
  jQuery
}

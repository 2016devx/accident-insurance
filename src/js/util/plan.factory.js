/**
 * 套餐工厂模块
 * @date 2017-11-20
 */
var planFactory = (planno) => {
  switch (planno) {
    case 'LACP0001' :
      return {
        // 一般医疗保险金
        commonMedicalTraementInsuranceMoney: '',
        // 扩展医疗保险金
        extendedMedicalTraementInsuranceMoney: '1万',
        // 额外医疗保险金
        extraMedicalTraementInsuranceMoney: ''
      }
    case 'LACP0002':
      return {
        // 一般医疗保险金
        commonMedicalTraementInsuranceMoney: '',
        // 扩展医疗保险金
        extendedMedicalTraementInsuranceMoney: '2万',
        // 额外医疗保险金
        extraMedicalTraementInsuranceMoney: ''
      }
    case 'LACP0003':
      return {
        // 一般医疗保险金
        commonMedicalTraementInsuranceMoney: '',
        // 扩展医疗保险金
        extendedMedicalTraementInsuranceMoney: '3万',
        // 额外医疗保险金
        extraMedicalTraementInsuranceMoney: ''
      }
  }
}

export default planFactory

var rDate = /(\d{4})-(\d{1,2})-(\d{1,2})/

var normalizeDate = (s) => {
  return s.replace(rDate, ($1, $2, $3, $4) => {
    // 分配一个数组
    var arr
    // 修正月份为双数字格式
    if ($3.length === 1) {
      $3 = '0' + $3
    }
    // 修正日为双数字格式
    if ($4.length === 1) {
      $4 = '0' + $4
    }
    arr = [$2, $3, $4]
    return arr.join('-')
  })
}

export {
  normalizeDate
}

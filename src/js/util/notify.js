import alertify from '@/plugins/alertify'

// 通知错误
var notify = (s, status = 'error') => {
  alertify.notify(
    s,
    status, // 可选配置 'success'
    3
  )
}

export default notify

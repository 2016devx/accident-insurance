/**
* 正则表达式模块
 * @date 2017-11-22
 *
 * @ref
 * 正则表达式由[http://tool.chinaz.com/regex/]网站提供
 */

// 汉字正则
export var rChinese = /^[\u4e00-\u9fa5]{2,}$/

// 身份证正则
export var rIdentity = /^\d{17}[\d|x]$|^\d{15}$/

// 国内手机号码正则
export var rPhone = /^1\d{10}$/

// 邮箱地址正则
export var rEmail = /^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}$/

/**
 * 保额工厂模块
 * @date 2017-11-22
 */
var sumInsuredFactory = (planno) => {
  switch (planno) {
    case 'LACP0001':
      return 11
    case 'LACP0002':
      return 22
    case 'LACP0003':
      return 33
  }
}

export default sumInsuredFactory

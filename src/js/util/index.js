/**
 * 工具库模块
 */
import { jQuery } from './extend'
import { normalizeDate } from './date'

var extend = jQuery.extend

// 切换样式并且进行相应的数据绑定
var link = ($event, vm, prop) => {
  // @Note
  // 后端很可能用0代表false
  vm[prop] = !vm[prop]
  $event.target.classList.toggle('is-important')
}

// 可购买安心疗的最大日期
var generateMaxDate = () => {
  return new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()
}

export {
  link
  , normalizeDate
  , generateMaxDate
  , extend
}

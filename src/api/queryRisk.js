import {
  requestBodyBuilder,
  INSURAER,
  ajax
} from './api.helper'

import {
  suffix
} from './ajax.const'

var queryRisk = () => {
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_RISKS'
      },
      body: {
        insurer: INSURAER
      }
    }
  })

  return ajax(`/life/risks?type=ac${suffix}`, data)
}

export default queryRisk

import {
  requestBodyBuilder,
  INSURAER,
  ajax
} from './api.helper'

import {
  suffix
} from './ajax.const'

var queryProfession = (options) => {
  options = options || {}
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_PROFRESSION'
      },
      body: {
        insurer: INSURAER,
        parentno: options.parentno || '0'
      }
    }
  })

  return ajax(`/life/profession?type=ac${suffix}`, data)
}

export default queryProfession

import {
  requestBodyBuilder,
  INSURAER,
  ajax
} from './api.helper'

import {
  suffix
} from './ajax.const'

var queryInsuranceDetail = (options) => {
  options = options || {}
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_INSURANCE_INFO'
      },
      body: {
        insurer: INSURAER,
        insuranceno: options.insuranceno
      }
    }
  })

  return ajax(`/life/insureinfo?type=ac${suffix}`, data)
}

export default queryInsuranceDetail

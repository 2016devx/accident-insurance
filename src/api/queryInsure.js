import {
  requestBodyBuilder,
  INSURAER,
  ajax
} from './api.helper'

import {
  suffix
} from './ajax.const'

var queryInsure = (options) => {
  options = options || {}
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_INSURE'
      },
      body: {
        insurer: INSURAER,
        transactionid: options.transactionid,
        applicant_name: options.applicant_name,
        applicant_identity: options.applicant_identity,
        applicant_phone: options.applicant_phone,
        applicant_email: options.applicant_email,
        insured_profession: options.insured_profession,
        relative: options.relative,
        insured_name: options.insured_name,
        insured_identity: options.insured_identity
      }
    }
  })

  return ajax(`/life/insure?type=ac${suffix}`, data)
}

export default queryInsure

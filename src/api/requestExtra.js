/**
 * 请求附加条款
 */

import {
  ajax
} from './api.helper'

import {
  suffix
} from './ajax.const'

var url = `http://open.xjt365.cn/v1/life/doc?type=ac&insurer=CHINALIFE&f=8${suffix}`

var query = (options) => {
  return ajax(url, {})
}

export default query

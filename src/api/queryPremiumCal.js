import {
  requestBodyBuilder,
  INSURAER,
  ajax
} from './api.helper'

import {
  suffix
} from './ajax.const'

var queryPremiumCal = (options) => {
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_PREMIUM_CAL'
      },
      body: {
        birthday: options.birthday,
        planno: options.planno,
        insurer: INSURAER
      }
    }
  })

  return ajax(`/life/premiumcal?type=ac${suffix}`, data)
}

export default queryPremiumCal

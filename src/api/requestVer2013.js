/**
 * 请求2013版本条款
 */

import {
  ajax
} from './api.helper'

import {
  suffix
} from './ajax.const'

var url = `http://open.xjt365.cn/v1/life/doc?type=ac&insurer=CHINALIFE&f=7${suffix}`

var query = (options) => {
  return ajax(url, {})
}

export default query

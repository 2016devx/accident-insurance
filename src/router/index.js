import Vue from 'vue'
import Router from 'vue-router'

// Vuex实例
import store from '@/store'

// // 入口模块不使用异步加载
// import Entry from '@/components/Entry'

const Entry = resolve => require.ensure([], () => resolve(require('@/components/Entry.vue')))

const QueryJobClass = resolve => require.ensure([], () => resolve(require('@/components/QueryJobClass.vue')))
const HealthAnnouncement = resolve => require.ensure([], () => resolve(require('@/components/HealthAnnouncement.vue')))
const InsuranceInfo = resolve => require.ensure([], () => resolve(require('@/components/InsuranceInfo.vue')))
const ClientAnnouncement = resolve => require.ensure([], () => resolve(require('@/components/ClientAnnouncement.vue')))
const InsuranceDetail = resolve => require.ensure([], () => resolve(require('@/components/InsuranceDetail.vue')))

// 应用程序都在这里安装插件
Vue.use(Router)

const router = new Router({
  // 路由配置
  routes: [
    {
      path: '/',
      name: 'Entry',
      component: Entry,
      // [重定向与域名](https://router.vuejs.org/zh-cn/essentials/redirect-and-alias.html)
      // 为默认路由设置别名
      alias: '/entry'
    },
    {
      path: '/queryJobClass',
      name: 'QueryJobClass',
      component: QueryJobClass
    },
    {
      path: '/healthAnnouncement',
      name: 'HealthAnnouncement',
      component: HealthAnnouncement
    },
    {
      path: '/insuranceInfo',
      name: 'insuranceInfo',
      component: InsuranceInfo
    },
    {
      path: '/clientAnnouncement',
      name: 'ClientAnnouncement',
      component: ClientAnnouncement
    },
    {
      path: '/insuranceDetail',
      name: 'InsuranceDetail',
      component: InsuranceDetail
    }
  ],

  // [滚动行为配置](https://router.vuejs.org/zh-cn/advanced/scroll-behavior.html)
  scrollBehavior (to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    }
  }
})

// 使用Hashmap比使用或运算符判断更快
let routeHashmap = {
  '/healthAnnouncement': true,
  '/insuranceInfo': true,
  '/clientAnnouncement': true,
  '/queryJobClass': true,
  '/insuranceDetail': true
}

// [导航守卫](https://router.vuejs.org/zh-cn/advanced/navigation-guards.html)
router.beforeEach((to, from, next) => {
  // 当前路径
  var p = to.path
  // 避免用户进行非法操作
  if (routeHashmap[p] && store.state.transactionid === '') {
    router.push('/')
  }

  // 进行管道中的下一个钩子
  next()
})

export default router

/* eslint-disable */
/**
 * alertifyjs 1.11.0 http://alertifyjs.com
 * Licensed under GPL 3 <https://opensource.org/licenses/gpl-3.0>*/
'use strict';

/**
 * Keys enum
 * @type {Object}
 */
var keys = {
    ENTER: 13,
    ESC: 27,
    F1: 112,
    F12: 123,
    LEFT: 37,
    RIGHT: 39
};
/**
 * Default options
 * @type {Object}
 */
var defaults = {
    autoReset:true,
    basic:false,
    closable:true,
    closableByDimmer:true,
    frameless:false,
    maintainFocus:true, //global default not per instance, applies to all dialogs
    maximizable:true,
    modal:true,
    movable:true,
    moveBounded:false,
    overflow:true,
    padding: true,
    pinnable:true,
    pinned:true,
    preventBodyShift:false, //global default not per instance, applies to all dialogs
    resizable:true,
    startMaximized:false,
    transition:'pulse',
    notifier:{
        delay:5,
        position:'bottom-right',
        closeButton:false
    },
    glossary:{
        title:'AlertifyJS',
        ok: 'OK',
        cancel: 'Cancel',
        acccpt: 'Accept',
        deny: 'Deny',
        confirm: 'Confirm',
        decline: 'Decline',
        close: 'Close',
        maximize: 'Maximize',
        restore: 'Restore',
    },
    theme:{
        input:'ajs-input',
        ok:'ajs-ok',
        cancel:'ajs-cancel',
    }
};

//holds open dialogs instances
var openDialogs = [];

/**
 * [Helper]  Adds the specified class(es) to the element.
 *
 * @element {node}      The element
 * @className {string}  One or more space-separated classes to be added to the class attribute of the element.
 *
 * @return {undefined}
 */
function addClass(element,classNames){
    element.className += ' ' + classNames;
}

/**
 * [Helper]  Removes the specified class(es) from the element.
 *
 * @element {node}      The element
 * @className {string}  One or more space-separated classes to be removed from the class attribute of the element.
 *
 * @return {undefined}
 */
function removeClass(element, classNames) {
    var original = element.className.split(' ');
    var toBeRemoved = classNames.split(' ');
    for (var x = 0; x < toBeRemoved.length; x += 1) {
        var index = original.indexOf(toBeRemoved[x]);
        if (index > -1){
            original.splice(index,1);
        }
    }
    element.className = original.join(' ');
}

/**
 * [Helper]  Checks if the document is RTL
 *
 * @return {Boolean} True if the document is RTL, false otherwise.
 */
function isRightToLeft(){
    return window.getComputedStyle(document.body).direction === 'rtl';
}
/**
 * [Helper]  Get the document current scrollTop
 *
 * @return {Number} current document scrollTop value
 */
function getScrollTop(){
    return ((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop);
}

/**
 * [Helper]  Get the document current scrollLeft
 *
 * @return {Number} current document scrollLeft value
 */
function getScrollLeft(){
    return ((document.documentElement && document.documentElement.scrollLeft) || document.body.scrollLeft);
}

/**
* Helper: clear contents
*
*/
function clearContents(element){
    while (element.lastChild) {
        element.removeChild(element.lastChild);
    }
}
/**
 * Extends a given prototype by merging properties from base into sub.
 *
 * @sub {Object} sub The prototype being overwritten.
 * @base {Object} base The prototype being written.
 *
 * @return {Object} The extended prototype.
 */
function copy(src) {
    if(null === src){
        return src;
    }
    var cpy;
    if(Array.isArray(src)){
        cpy = [];
        for(var x=0;x<src.length;x+=1){
            cpy.push(copy(src[x]));
        }
        return cpy;
    }

    if(src instanceof Date){
        return new Date(src.getTime());
    }

    if(src instanceof RegExp){
        cpy = new RegExp(src.source);
        cpy.global = src.global;
        cpy.ignoreCase = src.ignoreCase;
        cpy.multiline = src.multiline;
        cpy.lastIndex = src.lastIndex;
        return cpy;
    }

    if(typeof src === 'object'){
        cpy = {};
        // copy dialog pototype over definition.
        for (var prop in src) {
            if (src.hasOwnProperty(prop)) {
                cpy[prop] = copy(src[prop]);
            }
        }
        return cpy;
    }
    return src;
}
/**
  * Helper: destruct the dialog
  *
  */
function destruct(instance, initialize){
    //delete the dom and it's references.
    var root = instance.elements.root;
    root.parentNode.removeChild(root);
    delete instance.elements;
    //copy back initial settings.
    instance.settings = copy(instance.__settings);
    //re-reference init function.
    instance.__init = initialize;
    //delete __internal variable to allow re-initialization.
    delete instance.__internal;
}

/**
 * Use a closure to return proper event listener method. Try to use
 * `addEventListener` by default but fallback to `attachEvent` for
 * unsupported browser. The closure simply ensures that the test doesn't
 * happen every time the method is called.
 *
 * @param    {Node}     el    Node element
 * @param    {String}   event Event type
 * @param    {Function} fn    Callback of event
 * @return   {Function}
 */
var on = (function () {
    if (document.addEventListener) {
        return function (el, event, fn, useCapture) {
            el.addEventListener(event, fn, useCapture === true);
        };
    } else if (document.attachEvent) {
        return function (el, event, fn) {
            el.attachEvent('on' + event, fn);
        };
    }
}());

/**
 * Use a closure to return proper event listener method. Try to use
 * `removeEventListener` by default but fallback to `detachEvent` for
 * unsupported browser. The closure simply ensures that the test doesn't
 * happen every time the method is called.
 *
 * @param    {Node}     el    Node element
 * @param    {String}   event Event type
 * @param    {Function} fn    Callback of event
 * @return   {Function}
 */
var off = (function () {
    if (document.removeEventListener) {
        return function (el, event, fn, useCapture) {
            el.removeEventListener(event, fn, useCapture === true);
        };
    } else if (document.detachEvent) {
        return function (el, event, fn) {
            el.detachEvent('on' + event, fn);
        };
    }
}());

/**
 * Prevent default event from firing
 *
 * @param  {Event} event Event object
 * @return {undefined}

function prevent ( event ) {
    if ( event ) {
        if ( event.preventDefault ) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    }
}
*/
var transition = (function () {
    var t, type;
    var supported = false;
    var transitions = {
        'animation'        : 'animationend',
        'OAnimation'       : 'oAnimationEnd oanimationend',
        'msAnimation'      : 'MSAnimationEnd',
        'MozAnimation'     : 'animationend',
        'WebkitAnimation'  : 'webkitAnimationEnd'
    };

    for (t in transitions) {
        if (document.documentElement.style[t] !== undefined) {
            type = transitions[t];
            supported = true;
            break;
        }
    }

    return {
        type: type,
        supported: supported
    };
}());

/**
* Creates event handler delegate that sends the instance as last argument.
*
* @return {Function}    a function wrapper which sends the instance as last argument.
*/
function delegate(context, method) {
    return function () {
        if (arguments.length > 0) {
            var args = [];
            for (var x = 0; x < arguments.length; x += 1) {
                args.push(arguments[x]);
            }
            args.push(context);
            return method.apply(context, args);
        }
        return method.apply(context, [null, context]);
    };
}
/**
* Helper for creating a dialog close event.
*
* @return {object}
*/
function createCloseEvent(index, button) {
    return {
        index: index,
        button: button,
        cancel: false
    };
}
/**
* Helper for dispatching events.
*
* @param  {string} evenType The type of the event to disptach.
* @param  {object} instance The dialog instance disptaching the event.
*
* @return   {any}   The result of the invoked function.
*/
function dispatchEvent(eventType, instance) {
    if ( typeof instance.get(eventType) === 'function' ) {
        return instance.get(eventType).call(instance);
    }
}


var notifier = (function () {
    var reflow,
        element,
        openInstances = [],
        classes = {
            base: 'alertify-notifier',
            message: 'ajs-message',
            top: 'ajs-top',
            right: 'ajs-right',
            bottom: 'ajs-bottom',
            left: 'ajs-left',
            center: 'ajs-center',
            visible: 'ajs-visible',
            hidden: 'ajs-hidden',
            close: 'ajs-close'
        };
    /**
     * Helper: initializes the notifier instance
     *
     */
    function initialize(instance) {

        if (!instance.__internal) {
            instance.__internal = {
                position: alertify.defaults.notifier.position,
                delay: alertify.defaults.notifier.delay,
            };

            element = document.createElement('DIV');

            updatePosition(instance);
        }

        //add to DOM tree.
        if (element.parentNode !== document.body) {
            document.body.appendChild(element);
        }
    }

    function pushInstance(instance) {
        instance.__internal.pushed = true;
        openInstances.push(instance);
    }
    function popInstance(instance) {
        openInstances.splice(openInstances.indexOf(instance), 1);
        instance.__internal.pushed = false;
    }
    /**
     * Helper: update the notifier instance position
     *
     */
    function updatePosition(instance) {
        element.className = classes.base;
        switch (instance.__internal.position) {
        case 'top-right':
            addClass(element, classes.top + ' ' + classes.right);
            break;
        case 'top-left':
            addClass(element, classes.top + ' ' + classes.left);
            break;
        case 'top-center':
            addClass(element, classes.top + ' ' + classes.center);
            break;
        case 'bottom-left':
            addClass(element, classes.bottom + ' ' + classes.left);
            break;
        case 'bottom-center':
            addClass(element, classes.bottom + ' ' + classes.center);
            break;

        default:
        case 'bottom-right':
            addClass(element, classes.bottom + ' ' + classes.right);
            break;
        }
    }

    /**
    * creates a new notification message
    *
    * @param  {DOMElement} message	The notifier message element
    * @param  {Number} wait   Time (in ms) to wait before the message is dismissed, a value of 0 means keep open till clicked.
    * @param  {Function} callback A callback function to be invoked when the message is dismissed.
    *
    * @return {undefined}
    */
    function create(div, callback) {

        function clickDelegate(event, instance) {
            if(!instance.__internal.closeButton || event.target.getAttribute('data-close') === 'true'){
                instance.dismiss(true);
            }
        }

        function transitionDone(event, instance) {
            // unbind event
            off(instance.element, transition.type, transitionDone);
            // remove the message
            element.removeChild(instance.element);
        }

        function initialize(instance) {
            if (!instance.__internal) {
                instance.__internal = {
                    pushed: false,
                    delay : undefined,
                    timer: undefined,
                    clickHandler: undefined,
                    transitionEndHandler: undefined,
                    transitionTimeout: undefined
                };
                instance.__internal.clickHandler = delegate(instance, clickDelegate);
                instance.__internal.transitionEndHandler = delegate(instance, transitionDone);
            }
            return instance;
        }
        function clearTimers(instance) {
            clearTimeout(instance.__internal.timer);
            clearTimeout(instance.__internal.transitionTimeout);
        }
        return initialize({
            /* notification DOM element*/
            element: div,
            /*
              * Pushes a notification message
              * @param {string or DOMElement} content The notification message content
              * @param {Number} wait The time (in seconds) to wait before the message is dismissed, a value of 0 means keep open till clicked.
              *
              */
            push: function (_content, _wait) {
                if (!this.__internal.pushed) {

                    pushInstance(this);
                    clearTimers(this);

                    var content, wait;
                    switch (arguments.length) {
                    case 0:
                        wait = this.__internal.delay;
                        break;
                    case 1:
                        if (typeof (_content) === 'number') {
                            wait = _content;
                        } else {
                            content = _content;
                            wait = this.__internal.delay;
                        }
                        break;
                    case 2:
                        content = _content;
                        wait = _wait;
                        break;
                    }
                    this.__internal.closeButton = alertify.defaults.notifier.closeButton;
                    // set contents
                    if (typeof content !== 'undefined') {
                        this.setContent(content);
                    }
                    // append or insert
                    if (notifier.__internal.position.indexOf('top') < 0) {
                        element.appendChild(this.element);
                    } else {
                        element.insertBefore(this.element, element.firstChild);
                    }
                    reflow = this.element.offsetWidth;
                    addClass(this.element, classes.visible);
                    // attach click event
                    on(this.element, 'click', this.__internal.clickHandler);
                    return this.delay(wait);
                }
                return this;
            },
            /*
              * {Function} callback function to be invoked before dismissing the notification message.
              * Remarks: A return value === 'false' will cancel the dismissal
              *
              */
            ondismiss: function () { },
            /*
              * {Function} callback function to be invoked when the message is dismissed.
              *
              */
            callback: callback,
            /*
              * Dismisses the notification message
              * @param {Boolean} clicked A flag indicating if the dismissal was caused by a click.
              *
              */
            dismiss: function (clicked) {
                if (this.__internal.pushed) {
                    clearTimers(this);
                    if (!(typeof this.ondismiss === 'function' && this.ondismiss.call(this) === false)) {
                        //detach click event
                        off(this.element, 'click', this.__internal.clickHandler);
                        // ensure element exists
                        if (typeof this.element !== 'undefined' && this.element.parentNode === element) {
                            //transition end or fallback
                            this.__internal.transitionTimeout = setTimeout(this.__internal.transitionEndHandler, transition.supported ? 1000 : 100);
                            removeClass(this.element, classes.visible);

                            // custom callback on dismiss
                            if (typeof this.callback === 'function') {
                                this.callback.call(this, clicked);
                            }
                        }
                        popInstance(this);
                    }
                }
                return this;
            },
            /*
              * Delays the notification message dismissal
              * @param {Number} wait The time (in seconds) to wait before the message is dismissed, a value of 0 means keep open till clicked.
              *
              */
            delay: function (wait) {
                clearTimers(this);
                this.__internal.delay = typeof wait !== 'undefined' && !isNaN(+wait) ? +wait : notifier.__internal.delay;
                if (this.__internal.delay > 0) {
                    var  self = this;
                    this.__internal.timer = setTimeout(function () { self.dismiss(); }, this.__internal.delay * 1000);
                }
                return this;
            },
            /*
              * Sets the notification message contents
              * @param {string or DOMElement} content The notification message content
              *
              */
            setContent: function (content) {
                if (typeof content === 'string') {
                    clearContents(this.element);
                    this.element.innerHTML = content;
                } else if (content instanceof window.HTMLElement && this.element.firstChild !== content) {
                    clearContents(this.element);
                    this.element.appendChild(content);
                }
                if(this.__internal.closeButton){
                    var close = document.createElement('span');
                    addClass(close, classes.close);
                    close.setAttribute('data-close', true);
                    this.element.appendChild(close);
                }
                return this;
            },
            /*
              * Dismisses all open notifications except this.
              *
              */
            dismissOthers: function () {
                notifier.dismissAll(this);
                return this;
            }
        });
    }

    //notifier api
    return {
        /**
         * Gets or Sets notifier settings.
         *
         * @param {string} key The setting name
         * @param {Variant} value The setting value.
         *
         * @return {Object}	if the called as a setter, return the notifier instance.
         */
        setting: function (key, value) {
            //ensure init
            initialize(this);

            if (typeof value === 'undefined') {
                //get
                return this.__internal[key];
            } else {
                //set
                switch (key) {
                case 'position':
                    this.__internal.position = value;
                    updatePosition(this);
                    break;
                case 'delay':
                    this.__internal.delay = value;
                    break;
                }
            }
            return this;
        },
        /**
         * [Alias] Sets dialog settings/options
         */
        set:function(key,value){
            this.setting(key,value);
            return this;
        },
        /**
         * [Alias] Gets dialog settings/options
         */
        get:function(key){
            return this.setting(key);
        },
        /**
         * Creates a new notification message
         *
         * @param {string} type The type of notification message (simply a CSS class name 'ajs-{type}' to be added).
         * @param {Function} callback  A callback function to be invoked when the message is dismissed.
         *
         * @return {undefined}
         */
        create: function (type, callback) {
            //ensure notifier init
            initialize(this);
            //create new notification message
            var div = document.createElement('div');
            div.className = classes.message + ((typeof type === 'string' && type !== '') ? ' ajs-' + type : '');
            return create(div, callback);
        },
        /**
         * Dismisses all open notifications.
         *
         * @param {Object} excpet [optional] The notification object to exclude from dismissal.
         *
         */
        dismissAll: function (except) {
            var clone = openInstances.slice(0);
            for (var x = 0; x < clone.length; x += 1) {
                var  instance = clone[x];
                if (except === undefined || except !== instance) {
                    instance.dismiss();
                }
            }
        }
    };
})();

/**
 * Alertify public API
 * This contains everything that is exposed through the alertify object.
 *
 * @return {Object}
 */
function Alertify() {

    // holds a references of created dialogs
    var dialogs = {};

    /**
     * Extends a given prototype by merging properties from base into sub.
     *
     * @sub {Object} sub The prototype being overwritten.
     * @base {Object} base The prototype being written.
     *
     * @return {Object} The extended prototype.
     */
    function extend(sub, base) {
        // copy dialog pototype over definition.
        for (var prop in base) {
            if (base.hasOwnProperty(prop)) {
                sub[prop] = base[prop];
            }
        }
        return sub;
    }


    /**
    * Helper: returns a dialog instance from saved dialogs.
    * and initializes the dialog if its not already initialized.
    *
    * @name {String} name The dialog name.
    *
    * @return {Object} The dialog instance.
    */
    function get_dialog(name) {
        var dialog = dialogs[name].dialog;
        //initialize the dialog if its not already initialized.
        if (dialog && typeof dialog.__init === 'function') {
            dialog.__init(dialog);
        }
        return dialog;
    }

    /**
     * Helper:  registers a new dialog definition.
     *
     * @name {String} name The dialog name.
     * @Factory {Function} Factory a function resposible for creating dialog prototype.
     * @transient {Boolean} transient True to create a new dialog instance each time the dialog is invoked, false otherwise.
     * @base {String} base the name of another dialog to inherit from.
     *
     * @return {Object} The dialog definition.
     */
    function register(name, Factory, transient, base) {
        var definition = {
            dialog: null,
            factory: Factory
        };

        //if this is based on an existing dialog, create a new definition
        //by applying the new protoype over the existing one.
        if (base !== undefined) {
            definition.factory = function () {
                return extend(new dialogs[base].factory(), new Factory());
            };
        }

        if (!transient) {
            //create a new definition based on dialog
            definition.dialog = extend(new definition.factory(), dialog);
        }
        return dialogs[name] = definition;
    }

    return {
        /**
         * Alertify defaults
         *
         * @type {Object}
         */
        defaults: defaults,
        /**
         * Dialogs factory
         *
         * @param {string}      Dialog name.
         * @param {Function}    A Dialog factory function.
         * @param {Boolean}     Indicates whether to create a singleton or transient dialog.
         * @param {String}      The name of the base type to inherit from.
         */
        dialog: function (name, Factory, transient, base) {

            // get request, create a new instance and return it.
            if (typeof Factory !== 'function') {
                return get_dialog(name);
            }

            if (this.hasOwnProperty(name)) {
                throw new Error('alertify.dialog: name already exists');
            }

            // register the dialog
            var definition = register(name, Factory, transient, base);

            if (transient) {

                // make it public
                this[name] = function () {
                    //if passed with no params, consider it a get request
                    if (arguments.length === 0) {
                        return definition.dialog;
                    } else {
                        var instance = extend(new definition.factory(), dialog);
                        //ensure init
                        if (instance && typeof instance.__init === 'function') {
                            instance.__init(instance);
                        }
                        instance['main'].apply(instance, arguments);
                        return instance['show'].apply(instance);
                    }
                };
            } else {
                // make it public
                this[name] = function () {
                    //ensure init
                    if (definition.dialog && typeof definition.dialog.__init === 'function') {
                        definition.dialog.__init(definition.dialog);
                    }
                    //if passed with no params, consider it a get request
                    if (arguments.length === 0) {
                        return definition.dialog;
                    } else {
                        var dialog = definition.dialog;
                        dialog['main'].apply(definition.dialog, arguments);
                        return dialog['show'].apply(definition.dialog);
                    }
                };
            }
        },
        /**
         * Close all open dialogs.
         *
         * @param {Object} excpet [optional] The dialog object to exclude from closing.
         *
         * @return {undefined}
         */
        closeAll: function (except) {
            var clone = openDialogs.slice(0);
            for (var x = 0; x < clone.length; x += 1) {
                var instance = clone[x];
                if (except === undefined || except !== instance) {
                    instance.close();
                }
            }
        },
        /**
         * Gets or Sets dialog settings/options. if the dialog is transient, this call does nothing.
         *
         * @param {string} name The dialog name.
         * @param {String|Object} key A string specifying a propery name or a collection of key/value pairs.
         * @param {Variant} value Optional, the value associated with the key (in case it was a string).
         *
         * @return {undefined}
         */
        setting: function (name, key, value) {

            if (name === 'notifier') {
                return notifier.setting(key, value);
            }

            var dialog = get_dialog(name);
            if (dialog) {
                return dialog.setting(key, value);
            }
        },
        /**
         * [Alias] Sets dialog settings/options
         */
        set: function(name,key,value){
            return this.setting(name, key,value);
        },
        /**
         * [Alias] Gets dialog settings/options
         */
        get: function(name, key){
            return this.setting(name, key);
        },
        /**
         * Creates a new notification message.
         * If a type is passed, a class name "ajs-{type}" will be added.
         * This allows for custom look and feel for various types of notifications.
         *
         * @param  {String | DOMElement}    [message=undefined]		Message text
         * @param  {String}                 [type='']				Type of log message
         * @param  {String}                 [wait='']				Time (in seconds) to wait before auto-close
         * @param  {Function}               [callback=undefined]	A callback function to be invoked when the log is closed.
         *
         * @return {Object} Notification object.
         */
        notify: function (message, type, wait, callback) {
            return notifier.create(type, callback).push(message, wait);
        },
        /**
         * Creates a new notification message.
         *
         * @param  {String}		[message=undefined]		Message text
         * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
         * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
         *
         * @return {Object} Notification object.
         */
        message: function (message, wait, callback) {
            return notifier.create(null, callback).push(message, wait);
        },
        /**
         * Creates a new notification message of type 'success'.
         *
         * @param  {String}		[message=undefined]		Message text
         * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
         * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
         *
         * @return {Object} Notification object.
         */
        success: function (message, wait, callback) {
            return notifier.create('success', callback).push(message, wait);
        },
        /**
         * Creates a new notification message of type 'error'.
         *
         * @param  {String}		[message=undefined]		Message text
         * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
         * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
         *
         * @return {Object} Notification object.
         */
        error: function (message, wait, callback) {
            return notifier.create('error', callback).push(message, wait);
        },
        /**
         * Creates a new notification message of type 'warning'.
         *
         * @param  {String}		[message=undefined]		Message text
         * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
         * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
         *
         * @return {Object} Notification object.
         */
        warning: function (message, wait, callback) {
            return notifier.create('warning', callback).push(message, wait);
        },
        /**
         * Dismisses all open notifications
         *
         * @return {undefined}
         */
        dismissAll: function () {
            notifier.dismissAll();
        }
    };
}
var alertify = new Alertify();

export default alertify;

